/**
 * Module dependencies.
 */

let assert = require('assert');
let index = require('../src/index');

/**
 * Tests.
 */

describe('iframe', () => {

    it('add an iframe', (done) => {
        index.addIframe('https://example.com/');
        const iframe = document.getElementById('on_boarding_iframe');
        // or using a port of Node's `assert` from a bundler like browserify:
        assert(iframe != null);
        done();
    })

});