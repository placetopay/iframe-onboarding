// message event handler (e is event object)
function handleMessage(e)
{
    // Check origin
    if (e.origin === 'https://127.0.0.1' || e.origin === 'https://onboarding.placetopay.ws') {
        // remove the iframe
        let someIframe = document.getElementById('on_boarding_iframe');
        someIframe.parentNode.removeChild(document.getElementById('on_boarding_iframe'));
    }
}

module.exports = {
    addIframe(url) {

        // create iframe
        let iframe = document.createElement('iframe');
        iframe.src = url;
        iframe.id = 'on_boarding_iframe';
        iframe.style = 'position:fixed; top:0px; left:0px; bottom:0px; right:0px; width:100%; height:100%; border:none; margin:0; padding:0; overflow:hidden; z-index:999999;';
        // add text
        let iframeMessage = document.createTextNode('Please change your browser');
        iframe.appendChild(iframeMessage);

        // add iframe to DOM
        document.body.appendChild(iframe);

        if (window.addEventListener) {
            window.addEventListener('message', handleMessage, false);
        } else if (window.attachEvent) { // ie8
            window.attachEvent('onmessage', handleMessage);
        }
    }
}
