#Iframe for OnBoarding by Evertec-Placetopay

With this library you can make a faster integration by using the api that allows you to start an identity validation and that is described [here](https://evertec-onboarding.stoplight.io/docs/onboarding-api-docs/OnBoarding.v1.yaml)

##Quick start

* Install with npm: 

    npm install @placetopay/iframe-on-boarding

##Usage from node
The value of the url parameter is the url delivered in the response when requesting an identity validation in the [api](https://evertec-onboarding.stoplight.io/docs/onboarding-api-docs/OnBoarding.v1.yaml/paths/~1request/post)
```
onboarding = require('@placetopay/iframe-on-boarding');
onboarding.addIframe(url);
```